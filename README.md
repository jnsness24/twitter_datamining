# ToDo List and interesting things
## own Tweets
- [x] Wordcounter of Tweets
- [x] Co-Occurreness of Tweets
- [x] Seperation Hashtags and normal terms


## Follower (& Friends)
- [ ] Activity and *Most Valuable* Follower
- [ ] Language of Follower
- [ ] Location of Follower (maybe only visible in Tweets)

## Tweets of Follower n Friends
- [ ] Sentiment Analyse (good vs bad tweets)
