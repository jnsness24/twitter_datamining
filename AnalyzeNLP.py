import PreProcessingNLP as prepr
import json
import DBmodel

tweets = []



with open('data.json') as file:
    '''
    reads the json (collected Twitter Data) and save tweets in Array
    '''
    jsonObject = json.load(file)
    for item in jsonObject:
	    tweets.append(item['text'])

# 1. List without RTs
tweets_without_retweets = prepr.strip_retweets(tweets)

# 2. Lists without RTs and categorized
hashtags_list = prepr.tokenize_metadata(tweets_without_retweets, prepr.hashtags_re)
urls_list = prepr.tokenize_metadata(tweets_without_retweets, prepr.urls_re)
at_list = prepr.tokenize_metadata(tweets_without_retweets, prepr.at_re)
emoji_list = prepr.tokenize_metadata(tweets_without_retweets, prepr.emoticon_re)

token_list = prepr.tokenize_words(tweets_without_retweets,emoji_list, at_list, hashtags_list, urls_list)

# 3. Lists filtered by stopwords
token_list_removed_stop_words = prepr.remove_stop_words(token_list)

# 4. Lists lemmatized
token_list_lemmatized = prepr.lemmatize(token_list_removed_stop_words)

# 5. Lists vectorized
token_list_vectorized = prepr.vectorization(token_list_lemmatized)
token_bigram_list_vectorized = prepr.vectorization_bigrams(token_list_lemmatized)

hashtags_list_vectorized, urls_list_vectorized, at_list_vectorized, emoji_list_vectorized = prepr.vectorization(
	hashtags_list), prepr.vectorization(urls_list), prepr.vectorization(at_list), prepr.vectorization(emoji_list)



data = {'emojis':emoji_list,'ats' : at_list, "urls": urls_list, "hashtags":hashtags_list,
         "prep_words":token_list_lemmatized}

datahandler = DBmodel.DBmodel()
print(data)
#datahandler.saveToJson(data)
#datahandler.connectToMongoDB(data)
#datahandler.writeFileToMongoDB()

print(emoji_list_vectorized)
print(at_list_vectorized)
print(urls_list_vectorized)
print(hashtags_list_vectorized)
print(token_list_vectorized)


# print("Lexical Diversity:")
# print(len(token_list_lemmatized)/ (len(set(token_list_lemmatized))))

# Modal Verbs gives a hint what genre the text is
# modals = ['can', 'could', 'may', 'might', 'must', 'will']
# fdist = nltk.FreqDist(token_list)
# for m in modals:
# 	print(m + ':', fdist[m])

