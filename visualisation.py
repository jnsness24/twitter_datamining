import SliceData
import vincent

listFromCounter = SliceData.cnt_terms_pure.most_common(20)

labels, freq = zip(*listFromCounter)

data = {'y': freq, 'x': labels}
bar = vincent.Bar(data, iter_idx='x')
bar.axis_titles(x='Terms',y='Amount')
bar.to_json('vis_termfreq_bardiagramm.json')

print(data)