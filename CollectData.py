import tweepy
from tweepy import OAuthHandler
import json
import csv


def connectToTwitter():
	'''
	setup connection to twitter | credentials are stored in version UNcontrolled csv-file
	@return: access
	'''

	credentials = []
	with open('credentials.csv', 'r') as f:
		reader = csv.reader(f)
		for row in reader:
			credentials.append(row[0])

	consumer_key = credentials[0]
	consumer_secret = credentials[1]
	access_token = credentials[2]
	access_secret = credentials[3]

	auth = OAuthHandler(consumer_key, consumer_secret)
	auth.set_access_token(access_token, access_secret)
	access = tweepy.API(auth)

	return access


api = connectToTwitter()
tweets = []


def printStatus():
    for status in tweepy.Cursor(api.home_timeline).items():
        print(status._json)

def printFollowers():
    for friend in tweepy.Cursor(api.friends).items():
        print(friend._json)


def getTweets():
	for tweet in tweepy.Cursor(api.user_timeline).items():
		tweets.append(tweet._json)


def saveTweetsToJsonFile():
	with open('data.json', 'w') as outfile:
		json.dump(tweets, outfile)



getTweets()
saveTweetsToJsonFile()
